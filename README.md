This is a sample project leveraging GPTScript to analyze files

## Usage

First run the container:

```
docker run -p 3000:3000 -e OPENAI_API_KEY=$OPENAI_API_KEY --name sentiments lucj/sentiments:v0.0.1
```

Then access the UI on [http://localhost:3000](http://localhost:3000)

## Status

WIP