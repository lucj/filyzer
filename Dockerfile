# Base stage for shared configuration
FROM node:20.11.1 as base
WORKDIR /app
COPY . .
ENV PORT 3000
EXPOSE 3000

# Development stage
FROM base as dev
ENV NODE_ENV=development
RUN npm install
CMD ["npm", "run", "dev"]

# Production stage
FROM base as production
RUN npm install
ENV NODE_ENV=production
RUN npm run build
CMD ["npm", "start"]
