"use client";

import { useRef, useState } from 'react';

export default function Home() {
  const [files, setFiles] = useState([]);
  const [analysisResults, setAnalysisResults] = useState([]);
  const [textInput, setTextInput] = useState(""); // State for text input content
  const [isProcessingSentiments, setIsProcessingSentiments] = useState(false);
  const fileInputRef = useRef(null); // Add a ref to the file input

  const getResultColor = (resultData) => {
    // Assuming resultData is a string and you're checking its content
    if (resultData.includes("Positive")) return "text-green-500";
    if (resultData.includes("Negative")) return "text-red-500";
    if (resultData.includes("Neutral")) return "text-blue-500";
    if (resultData.includes("Mixed")) return "text-orange-500";
    return "text-gray-800"; // Default color
  };

  const handleFileChange = (event) => {
    if (event.target.files.length) {
      setTextInput(""); // Clear text input when files are selected
    }
    setFiles([...event.target.files]);
    setAnalysisResults([]);
  };

  const handleTextInputChange = (event) => {
    if (event.target.value.trim().length) {
      clearFileSelection(); // Clear file selection when text is input
    }
    setTextInput(event.target.value);
  };

  const handleSentimentsAnalysis = async () => {
    setIsProcessingSentiments(true);
    setAnalysisResults([]);

    // Case 1: Text input is provided
    if (textInput.trim()) {
      try {
          const response = await fetch('/gpt/sentiments', {
              method: 'POST',
              headers: {
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify({ content: textInput.trim() }),
          });

          if (response.ok) {
              const result = await response.json();
              setAnalysisResults([{ name: 'Text input', data: result.data }]);
          } else {
              setAnalysisResults([{ name: 'Text input', data: 'Failed to analyze the text' }]);
          }
      } catch (error) {
          console.error('Error analyzing text:', error);
          setAnalysisResults([{ name: 'Text input', data: 'Error during sentiments analysis' }]);
      }
    } else if (files.length > 0) {

      for (const file of files) {
        const data = new FormData();
        data.append('file', file);

        try {
            const response = await fetch('/gpt/sentiments', {
                method: 'POST',
                body: data,
            });

            if (response.ok) {
                const result = await response.json();
                setAnalysisResults(prevResults => [...prevResults, { name: file.name, data: result.data }]);
            } else {
                setAnalysisResults(prevResults => [...prevResults, { name: file.name, data: 'Failed to analyze the file' }]);
            }
        } catch (error) {
            console.error('Error analyzing file:', error);
            setAnalysisResults(prevResults => [...prevResults, { name: file.name, data: 'Error during sentiments analysis' }]);
        }
      }
    }

    setIsProcessingSentiments(false);
  };

  const clearFileSelection = () => {
    if (fileInputRef.current) {
      fileInputRef.current.value = ""; // Clear the file input
    }
    setFiles([]); // Clear the files state
    setAnalysisResults([]); // Clear the analysis results state
  };

  return (
    <main className="flex flex-col justify-center items-center min-h-screen bg-gray-50">
      <div className="max-w-md mx-auto text-center p-4 shadow-lg rounded-lg bg-white">
      <h2 className="text-xl font-semibold my-2">Sentiments analysis</h2>
      <p className="text-md my-2">Enter some text or select a file</p>

      {/* Text input field */}
      <textarea
                value={textInput}
                onChange={handleTextInputChange}
                placeholder="Enter text for analysis..."
                className="mb-4 w-full p-2 border rounded"
              />

      {/* Custom file input */}
      <div className="flex justify-center items-center mb-2">

      <label className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded cursor-pointer min-w-[260px] inline-block text-center">
        File selection
        <input type="file" ref={fileInputRef} onChange={handleFileChange} multiple className="hidden" />
      </label>

      {files.length > 0 && (
        <button onClick={clearFileSelection} className="ml-2 bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
          Clear
        </button>
      )}
      </div>

        {/* List selected files */}
        {files.length > 0 && (
          <ul className="list-disc text-left mb-4 pl-4">
          {Array.from(files).map((file, index) => (
            <li key={index} className="pl-2">{file.name}</li> // Added pl-2 for additional left padding
          ))}
        </ul>
        )}

        {/* Sentiments button */}
        <button onClick={handleSentimentsAnalysis} disabled={!(files.length > 0 || textInput.trim()) || isProcessingSentiments} className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded disabled:bg-blue-300 my-2">
          {isProcessingSentiments ? 'Analyzing...' : 'Get sentiments'}
        </button>

        <div className="mt-4">
          <div className="text-left bg-gray-100 rounded p-4 overflow-auto">
            {analysisResults.length > 0 ? (
              analysisResults.map((result, index) => (
                <div key={index} className="flex justify-between">
                  { result.name && <span className="font-bold">{result.name}: </span> }
                  <span className={getResultColor(result.data)}>{result.data}</span>
                </div>
              ))
            ) : (
              <div>---</div>
            )}
          </div>
        </div>
      </div>
    </main>
  );
}
