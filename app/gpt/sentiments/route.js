const path = require('path');
const { execFile } = require('child_process');
const util = require('util');
const execFilePromise = util.promisify(execFile);
const gptScriptPath = path.resolve(__dirname, '../../../../../node_modules/@gptscript-ai/gptscript/bin/gptscript');

export async function POST(request) {
  try {

    // Handle text content
    if (request.headers.get('content-type')?.includes('application/json')) {
      const { content } = await request.json();
      const { stdout } = await execFilePromise(gptScriptPath, ["https://luc.run/sentiments.gpt", "--content", content]);

      return new Response(JSON.stringify({ data: stdout }), {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        },
      });

    // Handle file
    } else if (request.headers.get('content-type')?.includes('multipart/form-data')) {
      // TODO: handle txt and pdf files

      const data = await request.formData();
      const file = data.get('file'); // This should match the name attribute in your FormData.append() call
      console.log("File received: ", file.name);

      const bytes = await file.arrayBuffer()
      const buffer = Buffer.from(bytes)
      const text = buffer.toString('utf-8'); 
      const { stdout } = await execFilePromise(gptScriptPath, ["https://luc.run/sentiments.gpt", "--content", text]);

      return new Response(JSON.stringify({ data: stdout }), {
        status: 200,
        headers: {
          'Content-Type': 'application/json',
        },
      });
    } else {
      return new Response(JSON.stringify({ error: "Unsupported content type" }), {
        status: 500,
        headers: {
          'Content-Type': 'application/json',
        },
      });
    }

  } catch (error) {
    console.error(error);
    return new Response(JSON.stringify({ error: error.message }), {
      status: 500,
      headers: {
        'Content-Type': 'application/json',
      },
    });
  }
}
